## Semana 22 -  Carrierwave y Amazon

1. Introducción a Carrierwave
2. Setup del proyecto
3. Como funciona Carrierwave
4. Crear y montar Uploader
5. Subiendo el primer archivo
6. Fallback
7. Imagen de una URL remota
8. Subiendo archivos por consola
9. Transformando fotos con Minimagick
10. Generando versiones de fotos
11. Fallback versiones y Asset Pipeline
12. Quiz
13. Enctype
14. Amazon y S3
15. Iam
16. Guardando las variables de entorno
17. Fog
18. Paso a producción
19. Quiz

* Documentación Carrierwave: https://github.com/carrierwaveuploader/carrierwave
* https://www.rubydoc.info/gems/carrierwave/CarrierWave/MiniMagick

* Documentación MiniMagick: https://github.com/minimagick/minimagick

* Cambiar tamaño de fotos online: https://www.befunky.com/